package com.atlassian.bamboo.plugins.phing;

import com.atlassian.bamboo.v2.build.agent.capability.AbstractFileCapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;

import org.apache.log4j.Logger;
import org.apache.commons.lang.SystemUtils;
import org.jetbrains.annotations.NotNull;

public class PhingCapabilityHelper extends AbstractFileCapabilityDefaultsHelper
{
    private static final Logger log = Logger.getLogger(PhingCapabilityHelper.class);
    private static final String EXEC_NAME = "phing";

    @NotNull
    @Override
    protected String getExecutableName()
    {
        return SystemUtils.IS_OS_WINDOWS ? EXEC_NAME + ".exe" : EXEC_NAME;
    }

    @NotNull
    @Override
    protected String getCapabilityKey()
    {
        return CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + EXEC_NAME;
    }
}
