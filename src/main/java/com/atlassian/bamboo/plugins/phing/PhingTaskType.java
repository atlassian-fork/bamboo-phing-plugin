package com.atlassian.bamboo.plugins.phing;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.*;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.utils.process.ExternalProcess;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import java.util.List;

public class PhingTaskType implements CommonTaskType
{
    public static final String CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".phing";

    private final ProcessService processService;
    private final CapabilityContext capabilityContext;
    private final TestCollationService testCollationService;

    public PhingTaskType(final ProcessService processService,
                         final TestCollationService testCollationService,
                         CapabilityContext capabilityContext)
    {
        this.processService = processService;
        this.testCollationService = testCollationService;
        this.capabilityContext = capabilityContext;
    }

    @NotNull
    @Override
    public TaskResult execute(final CommonTaskContext commonTaskContext) throws TaskException
    {
        final ErrorMemorisingInterceptor errorLines = ErrorMemorisingInterceptor.newInterceptor();
        commonTaskContext.getBuildLogger().getInterceptorStack().add(errorLines);

        try
        {
            final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(commonTaskContext);
            final ConfigurationMap configurationMap = commonTaskContext.getConfigurationMap();
            final String phingPath = Preconditions.checkNotNull(
                capabilityContext.getCapabilityValue(
                    PhingTaskType.CAPABILITY_PREFIX + "." + configurationMap.get(PhingConfigurator.RUNTIME)
                ), "Builder path is not defined"
            );

            final String target = configurationMap.get(PhingConfigurator.TARGET);
            final String config = configurationMap.get(PhingConfigurator.CONFIG);
            final String arguments = configurationMap.get(PhingConfigurator.ARGUMENTS);
            final List<String> commandsList = Lists.newArrayList(phingPath, target);

            if (StringUtils.isNotBlank(arguments))
            {
                commandsList.add(arguments);
            }

            if (StringUtils.isNotBlank(config))
            {
                commandsList.add(String.format("-buildfile %s", config));
            }

            taskResultBuilder.checkReturnCode(createPhingProcess(commonTaskContext, commandsList));
            final TaskContext taskContext = Narrow.to(commonTaskContext, TaskContext.class);

            if (taskContext != null)
            {
                final String testParsingPattern = getTestParsingPattern(taskContext);
                final boolean hasTests = taskContext.getConfigurationMap()
                    .getAsBoolean(TaskConfigConstants.CFG_HAS_TESTS);

                if (hasTests && testParsingPattern != null)
                {
                    testCollationService.collateTestResults(taskContext, testParsingPattern);
                    taskResultBuilder.checkTestFailures();
                }
            }

            return taskResultBuilder.build();
        }
        finally
        {
            commonTaskContext.getCommonContext().getCurrentResult().addBuildErrors(errorLines.getErrorStringList());
        }
    }

    private ExternalProcess createPhingProcess(@NotNull CommonTaskContext taskContext, @NotNull List<String> commandsList)
    {
        return processService.executeExternalProcess(taskContext, new ExternalProcessBuilder()
            .commandFromString(StringUtils.join(commandsList, " "))
            .workingDirectory(taskContext.getWorkingDirectory())
        );
    }

    private String getTestParsingPattern(@NotNull TaskContext taskContext)
    {
        return taskContext.getConfigurationMap().get(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN);
    }
}
